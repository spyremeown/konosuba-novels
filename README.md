# The KonoSuba novels, now in \TeX.
The translation is from https://cgtranslations.me/konosuba/, buy them a coffee or support them on Patreon. 

You can do whatever with the __code__ (NOT the text itself, __I don't own the translation__) as long as you publish any changes you made back (code is under GPL, the contents are not).
I plan to do all the volumes and all the specials.
Clicking the Links will download the most up-to-date PDF from each volume

[Volume One](https://gitlab.com/spyremeown/konosuba-novels/-/raw/master/konosuba_volume_one/konosuba_one.pdf?inline=false)

[Volume Two](https://gitlab.com/spyremeown/konosuba-novels/-/raw/master/konosuba_volume_two/konosuba_two.pdf?inline=false)

[Volume Three](https://gitlab.com/spyremeown/konosuba-novels/-/raw/master/konosuba_volume_three/konosuba_three.pdf)

Cheers.

PS: I only add illustrations that are in pretty good quality. Only one from the short story on vol. three is missing rn, so I'll add as I find fit files.
